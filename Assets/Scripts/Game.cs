using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;


public class Game : MonoBehaviour
{
    IGameInput input;
    public PlayerController Player;
    public Camera Cam;

    public TMP_Text timeText;
    public Image slider;
    public float timeLimit = 60f;

    public Transform Goal;

    public GameObject ButtonRetry;
    public GameObject TextWon;
    public GameObject TextLost;
    public GameObject TextPaused;
    public Text TextHealth;

    float PlayerTrackingMinX = -3.0f;
    float PlayerTrackingMaxX;
    float PlayerTrackingMinY = 2.6f;
    float time;
    float timerMultiplierFactor;
    bool isPaused;
    float camDeltaX;
    float camDeltaY;
    bool gameOver;
    
    void Awake()
    {
        input = new GameInputSimpleKeyboard();
        AssignPlayer();
        AssignCamera();
        PlayerTrackingMaxX = Goal.transform.position.x - 3.0f;
        StartTimer();
    }

    void AssignPlayer()
    {
        if(Player == null)
            Player = FindObjectOfType<PlayerController>();
        if(Player == null)
            ErrorMissingComponent("player");
    }

    void AssignCamera()
    {
        if(Cam == null)
            Cam = Camera.main;
        if(Cam == null)
            ErrorMissingComponent("camera");

        camDeltaX = Cam.transform.position.x - PlayerTrackingMinX;
        camDeltaY = Cam.transform.position.y - 6.05f;
    }

    void ErrorMissingComponent(string label)
    {
        throw new Exception($"Couldn't find a {label} object on this level. Are you sure it exists in the scene {SceneManager.GetActiveScene().name}");
    }

    void Update()
    {
        if(input.IsPausePressed())
            TogglePause();

        if(isPaused || gameOver)
            return;

        TextHealth.text = $"Health: {(int)Player.Health}";
        Player.Move(input.GetMovementDirection(), input.IsJumpPressed(), input.IsCrouchPressed());
        UpdateTimer();

        if (Player.transform.position.x > Goal.transform.position.x && Player.transform.position.y > Goal.transform.position.y && Player.transform.position.z <1.5f  && Player.transform.position.z > -1.5f)
            Win();
        else if (Player.Health < 0 || time <= 0f || Player.transform.position.y < -8f)
            Lose();
    }

    //Making camera trail the player in LateUpdate because player's new position is ready by then
    void LateUpdate()
    {
        CameraUpdateTrailing();
    }

    void CameraUpdateTrailing()
    {
        float playerX = Player.transform.position.x;
        float playerY = Player.transform.position.y;
        var camTfm = Cam.transform;
        var camPos = camTfm.position;
        if (!(playerX < PlayerTrackingMinX || playerX > PlayerTrackingMaxX))
        {
            camPos.x = playerX + camDeltaX;
        }
        if (!(playerY < PlayerTrackingMinY))
        {
            camPos.y = playerY + camDeltaY;
        }

        camTfm.position = camPos;
    }

    void TogglePause()
    {
        SetPause(!isPaused);
    }

    void SetPause(bool pause)
    {
        PauseGame(pause);
        TextPaused.SetActive(pause);
    }

    void PauseGame(bool pause)
    {
        Time.timeScale = pause ? 0.0f : 1.0f;
        isPaused = pause;
    }

    private void StartTimer()
    {
        timerMultiplierFactor = 1f / timeLimit;
        time = timeLimit;

        timeText.text = timeLimit.ToString();
        slider.fillAmount = time * timerMultiplierFactor;
    }
    private void UpdateTimer()
    {
        if (!isPaused && time>0f)
        {
            time -= Time.deltaTime;
            timeText.text = Mathf.CeilToInt(time).ToString();
            slider.fillAmount = time * timerMultiplierFactor;
        }
    }

    void Win()
    {
        GameOver();
        TextWon.SetActive(true);
        ButtonRetry.SetActive(true);
    }

    void Lose()
    {
        GameOver();
        TextLost.SetActive(true);
        ButtonRetry.SetActive(true);
    }

    void GameOver()
    {
        gameOver = true;
        PauseGame(true);
    }

    public void RestartLevel()
    {
        PauseGame(false);
        var level = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(level);
    }
}
